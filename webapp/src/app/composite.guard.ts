// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {LoginService} from "./login.service";
import {map, switchMap, tap} from "rxjs/operators";
import {MakerApiService} from "./maker-api.service";
import {ToastrService} from "./toastr.service";

@Injectable({
    providedIn: 'root'
})
export class CompositeGuard implements CanActivate {

    constructor(private loginService: LoginService,
                private makerApi: MakerApiService,
                private toastr: ToastrService,
                private router: Router) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.loginService.checkLogin()
            .pipe(tap((ok) => {
                if (!ok) {
                    this.router.navigate(["/login"]);
                }
            }), switchMap((loginOk) => {
                if (loginOk && next.data.configRequired === true) {
                    console.log("Login is OK, checking configuration.");
                    return this.makerApi.getConfig()
                        .pipe(map((config) => {
                            if (!(config["binance.api.key"] && config["binance.api.secret"])) {
                                this.toastr.error("Incomplete Binance configuration. Redirecting to configuration page.");
                                this.router.navigate(["/config"]);
                                return false;
                            }
                            return true;
                        }))
                } else {
                    return of(loginOk);
                }
            }));
    }

}
