// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import {Injectable} from '@angular/core';
import {Observable, of, ReplaySubject} from "rxjs";
import {catchError, map, tap} from "rxjs/operators";
import {MakerApiService} from "./maker-api.service";
import {Router} from "@angular/router";

declare var localStorage: any;

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    $onLogin = new ReplaySubject<boolean>(1);

    authenticated = false;

    constructor(private makerApi: MakerApiService,
                private router: Router) {
    }

    private setAuthenticated() {
        if (!this.authenticated) {
            this.authenticated = true;
            this.$onLogin.next(true);
        }
    }

    checkLogin(): Observable<boolean> {
        if (this.authenticated) {
            return of(true);
        }

        const sessionId = localStorage.getItem("sessionId");
        if (sessionId !== null) {
            this.makerApi.setSessionId(sessionId);
        }

        return this.makerApi.get("/api/version")
            .pipe(map((response: any) => {
                this.setAuthenticated();
                return true;
            }), catchError((err) => {
                return of(false);
            }));
    }

    login(username: string, password: string): Observable<any> {
        return this.makerApi.login(username, password)
            .pipe(tap((response: any) => {
                localStorage.setItem("sessionId", response.sessionId);
                this.makerApi.setSessionId(response.sessionId);
                this.setAuthenticated();
            }));
    }

    private clearSessionId() {
        localStorage.removeItem("sessionId");
        this.makerApi.setSessionId(null);
    }

    gotoLogin() {
        this.router.navigate(["/login"])
            .then(() => {
                location.reload(true);
            });
    }

    logout() {
        this.clearSessionId();
        window.location.reload(true);
    }
}
