// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package server

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"gitlab.com/crankykernel/maker/go/auth"
	"gitlab.com/crankykernel/maker/go/config"
	"gitlab.com/crankykernel/maker/go/log"
	mathrand "math/rand"
	"net/http"
	"strings"
	"time"
)

func init() {
	mathrand.Seed(time.Now().UnixNano())
}

type Authenticator struct {
	username string
	password string
	sessions map[string]bool
}

func NewAuthenticator(configFilename string) *Authenticator {
	m := Authenticator{
		sessions: map[string]bool{},
	}

	m.username = config.GetString("username")
	if m.username == "" {
		m.username = "maker"
		config.Set("username", m.username)
	}

	showPassword := false
	password := config.GetString("password")
	if password != "" {
		m.password = password
	} else {
		password, m.password = m.generatePassword(configFilename)
		showPassword = true
	}

	if showPassword {
		fmt.Printf(`
A username and password have been generated for you. Please take note of them.
This is the one and only time the password will be available.

Username: %s
Password: %s

`, m.username, password)
	}

	return &m
}

func (m *Authenticator) hasSession(sessionId string) bool {
	val, ok := m.sessions[sessionId]
	if val && ok {
		return true
	}
	return false
}

func (m *Authenticator) generatePassword(configFilename string) (string, string) {
	password := m.getRandom(32)
	encoded, err := auth.EncodePassword(password)
	if err != nil {
		log.WithError(err).Fatalf("Failed to encode generated password")
	}
	config.Set("password", encoded)
	config.WriteConfig(configFilename)
	return password, encoded
}

func (m *Authenticator) getRandom(size int) string {
	alphanumerics := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
	b := make([]rune, size)
	for i := range b {
		b[i] = alphanumerics[mathrand.Intn(len(alphanumerics))]
	}
	return string(b)
}

func (m *Authenticator) requiresAuth(path string) bool {
	if strings.HasPrefix(path, "/api/login") {
		return false
	}
	if strings.HasPrefix(path, "/api") {
		return true
	}
	if strings.HasPrefix(path, "/ws") {
		return true
	}
	if strings.HasPrefix(path, "/proxy") {
		return true
	}
	return false
}

func (m *Authenticator) generateSessionId() (string, error) {
	bytes := make([]byte, 128)
	_, err := rand.Read(bytes)
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(bytes), nil
}

func (m *Authenticator) Login(username string, password string) (string, error) {
	if username != m.username {
		return "", fmt.Errorf("bad username")
	}
	ok, err := auth.CheckPassword(password, m.password)
	if err != nil {
		return "", err
	}
	if !ok {
		return "", fmt.Errorf("bad password")
	}
	sessionId, err := m.generateSessionId()
	if err != nil {
		return "", err
	}
	m.sessions[sessionId] = true
	return sessionId, nil
}

// Middleware function, which will be called for each request
func (m *Authenticator) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if !m.requiresAuth(r.URL.Path) {
			next.ServeHTTP(w, r)
			return
		}

		sessionId := r.FormValue("sessionId")
		if sessionId != "" {
			if m.hasSession(sessionId) {
				next.ServeHTTP(w, r)
				return
			}
		}

		sessionId = r.Header.Get("X-Session-ID")
		if sessionId != "" {
			if m.hasSession(sessionId) {
				next.ServeHTTP(w, r)
				return
			}
		}

		w.WriteHeader(http.StatusUnauthorized)
	})
}
