// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package config

import (
	"github.com/spf13/viper"
	"gitlab.com/crankykernel/maker/go/log"
	"sync"
)

var subscribers map[chan bool]bool
var lock sync.RWMutex

func init() {
	subscribers = make(map[chan bool]bool)
}

func Subscribe() chan bool {
	lock.Lock()
	defer lock.Unlock()
	channel := make(chan bool, 1)
	subscribers[channel] = true
	return channel
}

func Unsubscribe(channel chan bool) {
	lock.Lock()
	lock.Unlock()
	subscribers[channel] = false
	delete(subscribers, channel)
}

func WriteConfig(filename string) {
	viper.SetConfigFile(filename)
	log.Infof("Writing configuration file %s.", viper.ConfigFileUsed())
	viper.WriteConfig()
	lock.RLock()
	defer lock.RUnlock()
	for channel := range subscribers {
		select {
		case channel <- true:
		}
	}
}

func Set(key string, val string) {
	lock.Lock()
	defer lock.Unlock()
	viper.Set(key, val)
}

func SetFloat64(key string, val float64) {
	lock.Lock()
	defer lock.Unlock()
	viper.Set(key, val)
}

func GetString(key string) string {
	return viper.GetString(key)
}
