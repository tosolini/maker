// The MIT License (MIT)
//
// Copyright (c) 2018 Cranky Kernel
//
// Permission is hereby granted, free of charge, to any person
// obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without
// restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies
// of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
// BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package binanceex

import (
	"encoding/json"
	"fmt"
	"github.com/crankykernel/binanceapi-go"
	"gitlab.com/crankykernel/maker/go/log"
	"strings"
	"sync"
	"time"
)

type TradeStreamChannel chan binanceapi.StreamAggTrade

type TradeStreamManager struct {
	lock          sync.RWMutex
	subscriptions map[TradeStreamChannel]string
	streams       map[string]*binanceapi.Stream
	streamCount   map[string]int
}

func NewTradeStreamManager() *TradeStreamManager {
	return &TradeStreamManager{
		subscriptions: make(map[TradeStreamChannel]string),
		streams:       make(map[string]*binanceapi.Stream),
		streamCount:   make(map[string]int),
	}
}

func (m *TradeStreamManager) Subscribe(name string) TradeStreamChannel {
	m.lock.Lock()
	defer m.lock.Unlock()
	channel := make(TradeStreamChannel, 128)
	m.subscriptions[channel] = name
	return channel
}

func (m *TradeStreamManager) Unsubscribe(channel TradeStreamChannel) {
	m.lock.Lock()
	defer m.lock.Unlock()
	if _, exists := m.subscriptions[channel]; !exists {
		log.Errorf("Attempt to unsubscribe non existing channel")
	}
	delete(m.subscriptions, channel)
}

func (m *TradeStreamManager) AddSymbol(symbol string) {
	m.lock.Lock()
	defer m.lock.Unlock()
	symbol = strings.ToLower(symbol)
	_, exists := m.streamCount[symbol]
	if exists {
		m.streamCount[symbol] += 1
		return
	}
	m.streamCount[symbol] = 1
	go m.runStream(symbol)
}

func (m *TradeStreamManager) RemoveSymbol(symbol string) {
	m.lock.Lock()
	defer m.lock.Unlock()
	symbol = strings.ToLower(symbol)
	count, exists := m.streamCount[symbol]
	if !exists {
		return
	}
	if count > 1 {
		m.streamCount[symbol] -= 1
	} else {
		delete(m.streamCount, symbol)
	}
}

func (m *TradeStreamManager) streamRefCount(name string) int {
	m.lock.RLock()
	defer m.lock.RUnlock()
	count, exists := m.streamCount[name]
	if exists {
		return count
	}
	return 0
}

func (m *TradeStreamManager) runStream(name string) {
Retry:
	if m.streamRefCount(name) == 0 {
		return
	}
	streamName := fmt.Sprintf("%s@aggTrade", strings.ToLower(name))
	stream, err := binanceapi.OpenSingleStream(streamName)
	if err != nil {
		log.WithError(err).
			WithField("stream", streamName).
			Errorf("Failed to open trade stream")
		time.Sleep(1 * time.Second)
		goto Retry
	}
	log.WithFields(log.Fields{
		"symbol": name,
	}).Infof("Connected to trade Binance aggTrade stream")
	for {
		payload, err := stream.Next()
		if err != nil {
			log.WithError(err).
				WithField("stream", streamName).
				Errorf("Failed to read trade stream message")
			stream.Close()
			time.Sleep(1 * time.Second)
			goto Retry
		}

		// Check if we still have subscribers.
		count := m.streamRefCount(name)
		if count == 0 {
			log.WithFields(log.Fields{
				"tickerStream": name,
			}).Infof("Trade stream reference count is zero, disconnected stream")
			stream.Close()
			return
		}

		var trade binanceapi.StreamAggTrade
		if err := json.Unmarshal(payload, &trade); err != nil {
			log.WithError(err).WithFields(log.Fields{
				"name": name,
			}).Errorf("Failed to decode trade stream message")
			continue
		}

		m.lock.RLock()
		for channel := range m.subscriptions {
			select {
			case channel <- trade:
			default:
				log.Warnf("Failed to send Binance trade to channel [%s], would block",
					m.subscriptions[channel])
			}
		}
		m.lock.RUnlock()
	}
}
